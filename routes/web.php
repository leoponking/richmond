<?php
Route::redirect('/', '/admin/home');

Auth::routes(['register' => false]);

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.mass_destroy');
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', 'Admin\RolesController@massDestroy')->name('roles.mass_destroy');
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', 'Admin\UsersController@massDestroy')->name('users.mass_destroy');

    /**SubAdmin Route */
    Route::get('/subadmin', 'Admin\SubAdminController@index')->name('sub_admin.index');
    Route::get('/subadmin/create', 'Admin\SubAdminController@create')->name('sub_admin.create');
    Route::post('/subadmin', 'Admin\SubAdminController@store')->name('sub_admin.store');
    Route::get('/subadmin/edit/{id}', 'Admin\SubAdminController@edit')->name('sub_admin.edit');
    Route::post('/subadmin/{id}', 'Admin\SubAdminController@update')->name('sub_admin.update');
    Route::get('/subadmin/delete/{id}', 'Admin\SubAdminController@delete')->name('sub_admin.delete');

    /**Country Route */
    Route::get('/country', 'Admin\CountryController@index')->name('country.index');
    Route::get('/country/create', 'Admin\CountryController@create')->name('country.create');
    Route::post('/country', 'Admin\CountryController@store')->name('country.store');
    Route::get('/country/edit/{id}', 'Admin\CountryController@edit')->name('country.edit');
    Route::post('/country/{id}', 'Admin\CountryController@update')->name('country.update');
    Route::get('/country/delete/{id}', 'Admin\CountryController@delete')->name('country.delete');

     /**Language Route */
     Route::get('/language', 'Admin\LanguageController@index')->name('language.index');
     Route::get('/language/create', 'Admin\LanguageController@create')->name('language.create');
     Route::post('/language', 'Admin\LanguageController@store')->name('language.store');
     Route::get('/language/edit/{id}', 'Admin\LanguageController@edit')->name('language.edit');
     Route::post('/language/{id}', 'Admin\LanguageController@update')->name('language.update');
     Route::get('/language/delete/{id}', 'Admin\LanguageController@delete')->name('language.delete');

     /**Package Route */
     Route::get('/package', 'Admin\PackageController@index')->name('package.index');
     Route::get('/package/create', 'Admin\PackageController@create')->name('package.create');
     Route::post('/package', 'Admin\PackageController@store')->name('package.store');
     Route::get('/package/edit/{id}', 'Admin\PackageController@edit')->name('package.edit');
     Route::post('/package/{id}', 'Admin\PackageController@update')->name('package.update');
     Route::get('/package/delete/{id}', 'Admin\PackageController@delete')->name('package.delete');

     /**Module Permission Route */
     Route::get('/module_permission', 'Admin\ModulePermissionController@index')->name('module_permission.index');
     Route::get('/module_permission/create', 'Admin\ModulePermissionController@create')->name('module_permission.create');
     Route::post('/module_permission', 'Admin\ModulePermissionController@store')->name('module_permission.store');
     Route::get('/module_permission/edit/{id}', 'Admin\ModulePermissionController@edit')->name('module_permission.edit');
     Route::post('/module_permission/{id}', 'Admin\ModulePermissionController@update')->name('module_permission.update');
     Route::get('/module_permission/delete/{id}', 'Admin\ModulePermissionController@delete')->name('module_permission.delete');

      /**Package Route */
      Route::get('/module_permission', 'Admin\ModulePermissionController@index')->name('module_permission.index');
      Route::get('/module_permission/create', 'Admin\ModulePermissionController@create')->name('module_permission.create');
      Route::post('/module_permission', 'Admin\ModulePermissionController@store')->name('module_permission.store');
      Route::get('/module_permission/edit/{id}', 'Admin\ModulePermissionController@edit')->name('module_permission.edit');
      Route::post('/module_permission/{id}', 'Admin\ModulePermissionController@update')->name('module_permission.update');
      Route::get('/module_permission/delete/{id}', 'Admin\ModulePermissionController@delete')->name('module_permission.delete');
});
