<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Language;
use Illuminate\Support\Facades\Session;
class LanguageController extends Controller
{
    public function index(){
        $languages = Language::all();
        return view('admin.language.index',compact('languages'));
    }

    public function create(){
        return view('admin.language.create');
    }

    public function store(Request $request){
        $language = new Language;
        $language->name  = $request->name;
        $language->save();
        Session::flash('message','Language is Saved');
        return redirect()->route('admin.language.index');
    }

    public function edit($id){
        $language = Language::where('id',$id)->first();
        return view('admin.language.edit',compact('language'));
    }

    public function update($id, Request $request){
        $language = Language::where('id',$id)->first();   
        $language->update($request->all());
        return redirect()->route('admin.language.index')->withFlashSuccess('Language is Updated');
    }

    public function delete($id){
        Language::where('id',$id)->delete();
        return redirect()->route('admin.language.index')->withFlashSuccess('Language is Deleted');
    }
}
