<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ModulePermission;

class PackageController extends Controller
{
    public function index(){
        return view('admin.package.index');
    }

    public function create(){
        $modulePermissions = ModulePermission::pluck('name','id');
        return view('admin.package.create',compact('modulePermissions'));
    }
    public function store(Request $request){
        dd($request->all());
        $country = new Country;
        $country->sortname      = strtoupper($request->short_name);
        $country->name          = $request->name;
        $country->phone_code    = $request->phone_code;
        $country->save();
        return redirect()->route('admin.country.index')->withFlashSuccess('Country is Saved');
    }

    public function edit($id){
        $country = Country::where('id',$id)->first();
        return view('admin.country.edit',compact('country'));
    }

    public function update($id, Request $request){
        $country = Country::where('id',$id)->first();   
        $country->update($request->all());
        return redirect()->route('admin.country.index')->withFlashSuccess('Country is Updated');
    }

    public function delete($id){
        Country::where('id',$id)->delete();
        return redirect()->route('admin.country.index')->withFlashSuccess('Country is Deleted');
    }
}
