<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Language;
use App\User;
use App\Models\SubAdmin;
use Illuminate\Http\Request;

class SubAdminController extends Controller
{
    public function index(){
        $subadmins = SubAdmin::with('users','countries','languages')->get();
        return view('admin.sub-admin.index',compact('subadmins'));
    }

    public function create(){
        $countries = Country::all();
        $languages = Language::all();
        return view('admin.sub-admin.create',compact('countries','languages'));
    }

    public function store(Request $request){
        $user = new User;
        $user->name  = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt('password');
        $user->save();
        $subadmin = new SubAdmin;
        $subadmin->user_id        = $user->id;
        $subadmin->address        = $request->address;
        $subadmin->phone          = $request->phone;
        $subadmin->country_id     = $request->country_id;
        $subadmin->language_id     = $request->language_id;
        $subadmin->save();
        return redirect()->route('admin.sub_admin.index')->withFlashSuccess('Subadmin is Saved');
    }

    public function edit($id){
        $subadmin = SubAdmin::where('id',$id)->with('users','countries','languages')->first();
        $countries = Country::all();
        $languages = Language::all();
        return view('admin.sub-admin.edit',compact('subadmin','countries','languages'));
    }

    public function update($id, Request $request){
        $subadmin = Subadmin::where('id',$id)->first();   
        $user= User::where('id',$subadmin->user_id)->first();
        $user->update([
            'name'  => $request->name,
            'email' => $request->email
        ]);
        $subadmin->update([
            'address'    => $request->address,
            'phone'      => $request->phone,
            'country_id' => $request->country_id,
            'language_id'=> $request->language_id
        ]);
        return redirect()->route('admin.sub_admin.index')->withFlashSuccess('Subadmin is Updated');
    }

    public function delete($id){
        $subadmin = SubAdmin::where('id',$id)->first();
        User::where('id',$subadmin->user_id)->delete();
        $subadmin->delete();
        return redirect()->route('admin.sub_admin.index')->withFlashSuccess('Subadmin is Deleted');
    }
}
