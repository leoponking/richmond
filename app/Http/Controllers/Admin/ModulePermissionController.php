<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ModulePermission;

class ModulePermissionController extends Controller
{
    public function index(){
        $modulePermissions = ModulePermission::all();
        return view('admin.module-permission.index',compact('modulePermissions'));
    }

    public function create(){
        return view('admin.module-permission.create');
    }

    public function store(Request $request){
        $modulePermission = new ModulePermission;
        $modulePermission->name  = $request->name;
        $modulePermission->save();
        return redirect()->route('admin.module_permission.index')->withFlashSuccess('Module Permission is Saved');
    }

    public function edit($id){
        $modulePermission = ModulePermission::where('id',$id)->first();
        return view('admin.module-permission.edit',compact('modulePermission'));
    }

    public function update($id, Request $request){
        $modulePermission = ModulePermission::where('id',$id)->first();   
        $modulePermission->update($request->all());
        return redirect()->route('admin.module_permission.index')->withFlashSuccess('Module Permission is Updated');
    }

    public function delete($id){
        ModulePermission::where('id',$id)->delete();
        return redirect()->route('admin.module_permission.index')->withFlashSuccess('Module Permission is Deleted');
    }
}
