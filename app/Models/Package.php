<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'name',
        'price',
        'patient_limit',
        'doctor_limit',
        'mortuary_limit',
        'ambulance_limit',
        'counsellor_limit',
        'driver_limit',
        'module_permissions_id'
    ];
}