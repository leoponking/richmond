<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModulePermission extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name'
    ];
}
