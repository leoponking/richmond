<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class SubAdmin extends Model
{
    protected $fillable = [
        'user_id',
        'address',
        'phone',
        'country_id',
        'language_id'
    ];  

    public function users(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function countries(){
        return $this->belongsTo(Country::class,'country_id');
    }

    public function languages(){
        return $this->belongsTo(Language::class,'language_id');
    }
}
