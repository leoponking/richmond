@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        Add Country
    </div>
    <div class="card-body">
        <form action="{{route('admin.country.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Name*</label>
                        <input type="text" id="name" name="name" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="title">Short Name*</label>
                        <input type="text" id="short_name" name="short_name" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="title">Phone Code*</label>
                        <input type="number" id="phone_code" name="phone_code" class="form-control"  required>
                    </div>
                </div>
                <div  class="col-md-3">
                    <input class="btn btn-success" type="submit" value="{{ trans('global.save') }}">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection