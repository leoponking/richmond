@extends('layouts.admin')
@section('content')
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.country.create") }}">
            Add Country
        </a>
    </div>
</div>
<div class="card">
    <div class="card-header">
        All Countries
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Short Name</th>
                        <th>Name</th>                                                    
                        <th>Phone Code</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($countries as $country)
                    <tr>
                        <td>{{$country->sortname}}</td>
                        <td>{{$country->name}}</td>
                        <td>{{$country->phone_code}}</td>
                        <td>
                            <a type='button' href="{{ route('admin.country.edit',$country->id) }}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit&nbsp;</a>&nbsp;&nbsp;
                            <a type='button' href="{{ route('admin.country.delete',$country->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete&nbsp;</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection