@extends('layouts.admin')
@section('content')
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.module_permission.create") }}">
            Add Module Permissions
        </a>
    </div>
</div>
<div class="card">
    <div class="card-header">
        All Module Permissions
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th> Name</th>
                        <th> Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($modulePermissions as $modulePermission)
                    <tr>
                        <td>{{$modulePermission->name}}</td>
                        <td>
                            <a type='button' href="{{ route('admin.module_permission.edit',$modulePermission->id) }}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit&nbsp;</a>&nbsp;&nbsp;
                            <a type='button' href="{{ route('admin.module_permission.delete',$modulePermission->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete&nbsp;</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection