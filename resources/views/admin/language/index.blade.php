@extends('layouts.admin')
@section('content')
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.language.create") }}">
            Add Language
        </a>
    </div>
</div>
<div class="card">
    <div class="card-header">
        All Languages
    </div>

    <div class="card-body">
        <div class="table-responsive">
        @if(Session::has('message'))
        <div>{{Session::get('message')}}</div>
        @endif 
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th> Name</th>
                        <th> Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($languages as $language)
                    <tr>
                        <td>{{$language->name}}</td>
                        <td>
                            <a type='button' href="{{ route('admin.language.edit',$language->id) }}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit&nbsp;</a>&nbsp;&nbsp;
                            <a type='button' href="{{ route('admin.language.delete',$language->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete&nbsp;</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection