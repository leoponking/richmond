@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        Add Language
    </div>
    <div class="card-body">
        <form action="{{route('admin.language.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="title">Name*</label>
                        <input type="text" id="name" name="name" class="form-control"  required>
                    </div>
                </div>
                <div  class="col-md-3">
                    <input class="btn btn-success" type="submit" value="Update">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection