@extends('layouts.admin')
@section('content')
@can('users_manage')
<div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.sub_admin.create") }}">
                Add Sub Admin
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        All Sub Admins
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>                                                    
                        <th>Country</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($subadmins as $subadmin)
                    <tr>
                        <td>{{$subadmin->users->name}}</td>
                        <td>{{$subadmin->users->email}}</td>
                        <td>{{$subadmin->countries->name}}</td>
                        <td>{{$subadmin->languages->name}}</td>
                        <td>{{$subadmin->phone}}</td>
                        <td>
                            <a type='button' href="{{ route('admin.sub_admin.edit',$subadmin->id) }}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit&nbsp;</a>&nbsp;&nbsp;
                            <a type='button' href="{{ route('admin.sub_admin.delete',$subadmin->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete&nbsp;</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection