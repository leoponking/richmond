@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        Create Sub Admin
    </div>

    <div class="card-body">
        <form action="{{route('admin.sub_admin.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Name*</label>
                        <input type="text" id="name" name="name" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Email*</label>
                        <input type="text" id="email" name="email" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Address*</label>
                        <input type="text" id="address" name="address" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Phone*</label>
                        <input type="text" id="phone" name="phone" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Country*</label>
                        <select class="form-control" name="country_id"  required >
                            <option  value="">Select Country</option>
                        @foreach($countries as $key=>$country)
                            <option   value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Language*</label>
                        <select class="form-control" name="language_id" required >
                            <option  value="">Select Language</option>
                        @foreach($languages as $key=>$language)
                            <option   value="{{$language->id}}">{{$language->name}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection