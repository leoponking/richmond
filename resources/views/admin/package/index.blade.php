@extends('layouts.admin')
@section('content')

<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{route('admin.package.create')}}">
            Add Package
        </a>
    </div>
</div>
<div class="card">
    <div class="card-header">
        All Packages
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Package Name</th>
                        <th>Patient Limit</th>
                        <th>Doctor Limit</th>
                        <th>Mortuary Limit</th>
                        <th>Ambulance Limit</th>
                        <th>Driver Limit</th>
                        <th>Counselor Limit</th>
                        <th>Permitted Modules</th>
                        <th>Restricted Modules</th>
                        <th style="width:16%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                  
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a type='button' href="#" class="btn btn-sm btn-success"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit&nbsp;</a>&nbsp;&nbsp;
                            <a type='button' href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete&nbsp;</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection