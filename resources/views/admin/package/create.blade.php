@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        Add Package
    </div>
    <div class="card-body">
        <form action="{{route('admin.package.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Package Name*</label>
                        <input type="text" id="name" name="package_name" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Patient Limit*</label>
                        <input type="text" id="name" name="patient_limit" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Doctor Limit*</label>
                        <input type="text" id="name" name="doctor_limit" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Mortuary Limit*</label>
                        <input type="text" id="name" name="mortuary_limit" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Ambulance Limit*</label>
                        <input type="text" id="name" name="ambulance_limit" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Driver Limit*</label>
                        <input type="text" id="name" name="driver_limit" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Counsellor Limit*</label>
                        <input type="text" id="name" name="counsellor_limit" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Price*</label>
                        <input type="text" id="name" name="price" class="form-control"  required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="module_permissions">Module Permissions*</label>
                            <select class="form-control" name="module_permissions[]"  required multiple >
                            @foreach($modulePermissions as $key=>$modulePermissions)
                                <option   value="{{$key}}">{{$modulePermissions}}</option>
                            @endforeach
                            </select>
                </div>
                <div  class="col-md-3">
                    <input class="btn btn-success" type="submit" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection